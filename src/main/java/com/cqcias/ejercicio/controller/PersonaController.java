package com.cqcias.ejercicio.controller;

import com.cqcias.ejercicio.entity.Persona;
import com.cqcias.ejercicio.repository.PersonaRepository;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/persona")
public class PersonaController {

    private final PersonaRepository personaRepository;

    public PersonaController(PersonaRepository personaRepository) {
        this.personaRepository = personaRepository;
    }

    @GetMapping
    @CrossOrigin(origins = "http://localhost:4200")
    public List<Persona> getAll(){
        return personaRepository.getAll("1");
    }

    @GetMapping("/{id}")
    @CrossOrigin(origins = "http://localhost:4200")
    public Optional<Persona> getById(@PathVariable int id){
        return personaRepository.getById(id);
    }

}
