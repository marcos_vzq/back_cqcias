package com.cqcias.ejercicio.repository;

import com.cqcias.ejercicio.entity.Persona;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface PersonaRepository extends JpaRepository<Persona, Integer> {

    @Query("FROM Persona WHERE estatus = :estatus")
    List<Persona> getAll(@Param("estatus") String estatus);

    @Query("FROM Persona WHERE id = :id")
    Optional<Persona> getById(@Param("id") int id);

}
