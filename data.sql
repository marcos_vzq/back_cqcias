USE [CQCIAS]
GO
/****** Object:  Table [dbo].[Persona]    Script Date: 13/11/2020 01:23:14 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Persona](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[nombre] [varchar](100) NULL,
	[primer_apellido] [varchar](100) NULL,
	[segundo_apellido] [varchar](100) NULL,
	[telefono] [varchar](10) NULL,
	[estatus] [varchar](1) NULL,
 CONSTRAINT [PK_Persona] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Persona] ON 

INSERT [dbo].[Persona] ([id], [nombre], [primer_apellido], [segundo_apellido], [telefono], [estatus]) VALUES (1, N'Marcos', N'Vazquez', N'Gonzalez', N'1232343478', N'1')
INSERT [dbo].[Persona] ([id], [nombre], [primer_apellido], [segundo_apellido], [telefono], [estatus]) VALUES (2, N'Daniel', N'Perez', N'Garcia', N'8903245801', N'1')
INSERT [dbo].[Persona] ([id], [nombre], [primer_apellido], [segundo_apellido], [telefono], [estatus]) VALUES (3, N'Marisol', N'Estrada', N'Inojosa', N'4421234581', N'1')
INSERT [dbo].[Persona] ([id], [nombre], [primer_apellido], [segundo_apellido], [telefono], [estatus]) VALUES (4, N'Luis', N'Jimenez', N'Sandoval', N'4421234578', N'0')
SET IDENTITY_INSERT [dbo].[Persona] OFF
GO
